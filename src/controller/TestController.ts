/**
 * @author Francisco Roa <franroav@webkonce.cl>
 */
import { NextFunction, Request, Response } from "express";
import { testUserAccount } from "../tests/user/user.test";

export class testController {
  static testgetAllUserAccounts = async (req: Request, res: Response) => {};

  static testsaveNewUserAccount = async (req: Request, res: Response) => {};

  static testeditUserAccount = async (req: Request, res: Response) => {};

  static testreadUserCredits = async (req: Request, res: Response) => {};
}

export default testController;
