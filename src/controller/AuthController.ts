/**
 * @author Francisco Roa <franroav@webkonce.cl>
 */
import { Request, Response } from "express";
import { userClass } from "../clasess/userClass";

class AuthController {
  static login = async (req: Request, res: Response) => {
    const { CORREO } = req.body;

    if (!CORREO) {
      return res.status(400).json({ message: "el correo es requerido" });
    }
    try {
      const User = new userClass(
        "1vpjyo-LmrxZnzfCKRr2sSAW5NuJS1jO1A-yYSYeSVd8",
        req,
        res
      );

      const response = await User.validate_Login_Email(req, res);

      res.json(response);
    } catch (error) {
      console.log(error);
      res
        .status(400)
        .json({ message: "Algo ha ido mal porfavor intenlo nuevamente" });
    }
  };
}

export default AuthController;
