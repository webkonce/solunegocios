/**
 * @author Francisco Roa <franroav@webkonce.cl>
 */
import { NextFunction, Request, Response } from "express";
import { UserModel } from "../Model/UserModel";

export class UserController {
  static getAll = async (req: Request, res: Response) => {
    const allUsers = UserModel.getAll(req, res);
    return allUsers;
  };

  static saveNewUserAccount = async (req: Request, res: Response) => {
    const newAccount = UserModel.newAccount(req, res);
    return newAccount;
  };

  static editUserAccount = async (req: Request, res: Response) => {
    const editUser = UserModel.editUser(req, res);
    return editUser;
  };

  static readUserCredits = async (req: Request, res: Response) => {
    const getCredito = UserModel.getCreditsByEmail(req, res);
    return getCredito;
  };
}

export default UserController;
