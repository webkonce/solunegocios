/**
 * @author Francisco Roa <franroav@webkonce.cl>
 */
import { getRepository } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { User } from "../entity/User";
import { validate } from "class-validator";
import { userClass } from "../clasess/userClass";
import { Console } from "console";
const { GoogleSpreadsheet } = require("google-spreadsheet");

const credenciales = require("../config/json/credenciales.json");

let googleId = "1vpjyo-LmrxZnzfCKRr2sSAW5NuJS1jO1A-yYSYeSVd8";

export class UserModel {
  //GETALL USERS
  static getAll = async (req: Request, res: Response) => {
    const User = new userClass(
      "1vpjyo-LmrxZnzfCKRr2sSAW5NuJS1jO1A-yYSYeSVd8",
      req,
      res
    );

    const response = User.all_users(req, res);

    return response;
  };

  // SAVE NEW USER
  static newAccount = async (req: Request, res: Response) => {
    const User = new userClass(
      "1vpjyo-LmrxZnzfCKRr2sSAW5NuJS1jO1A-yYSYeSVd8",
      req,
      res
    );

    const response = User.saveNewUser(req, res);

    return response;
  };

  //UPDATE
  static editUser = async (req: Request, res: Response) => {
    const User = new userClass(
      "1vpjyo-LmrxZnzfCKRr2sSAW5NuJS1jO1A-yYSYeSVd8",
      req,
      res
    );

    const response = User.update_credit(req, res);

    return response;
  };

  static getCreditsByEmail = async (req: Request, res: Response) => {
    const User = new userClass(
      "1vpjyo-LmrxZnzfCKRr2sSAW5NuJS1jO1A-yYSYeSVd8",
      req,
      res
    );

    const response = User.read_credits(req, res);

    return response;
  };
}

export default UserModel;
