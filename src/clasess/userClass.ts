// GOOGLE SHEET
import { Request, Response } from "express";
import { User } from "../entity/User";
const { GoogleSpreadsheet } = require("google-spreadsheet");
const credenciales = require("../config/json/credenciales.json");
import * as jwt from "jsonwebtoken";
import config from "../config/config";

// GOOGLE SHEET SERVICE
class userService {
  //url: string;
  public valid: boolean = true;
  public validCreditNumber: boolean = true;
  private googleId: string;
  private req: Request;
  private res: Response;
  constructor(googleId: string, req: Request, res: Response) {
    this.googleId = googleId;
    this.req = req;
    this.res = res;
  }

  // ALL USERS
  async getAllUsers(req: Request, res: Response) {
    let Rows: User[] = [];
    let Headers = [];

    try {
      const documento = new GoogleSpreadsheet(this.googleId);
      await documento.useServiceAccountAuth(credenciales);
      await documento.loadInfo();

      const sheet = documento.sheetsByIndex[0];
      const registro = await sheet.getRows();
      const response = JSON.stringify(registro, this.getCircularReplacer());

      let json = JSON.parse(response);
      json.map((res, i) => {
        const { _rawData, _sheet, _rowNumber } = res;
        //const [col1, col2, col3] = _sheet.headerValues;
        const [CORREO, TIENDA] = _rawData;

        if (i === 0) {
          Headers = _sheet.headerValues;
        }
        // store
        const store = JSON.parse(TIENDA);
        Rows.push({ id: _rowNumber, CORREO, TIENDA: store });
      });
      res.send(Rows);
    } catch (error) {
      console.log(error);
    }
  }

  //SAVE NEW USER ACCOUNT
  async saveNewUser(req: Request, res: Response) {
    const { id, CORREO } = req.body;
    const stores = <string>req.headers["store"];

    const json = JSON.parse(stores);

    json.map((store) => {
      //console.log(Math.sign(store.credito));
      return Math.sign(store.credito) === -1
        ? (this.validCreditNumber = false)
        : true;
    });

    try {
      const documento = new GoogleSpreadsheet(this.googleId);
      await documento.useServiceAccountAuth(credenciales);
      await documento.loadInfo();

      const sheet = documento.sheetsByIndex[0];
      console.log({ CORREO, TIENDA: stores });

      if (!this.validCreditNumber) {
        res.status(400).json({
          message: "credito invalido!, no puede iniciar en negativo! (-)",
          error: "ERROR",
        });
        return;
      }
      // token
      const token = jwt.sign({ userId: id, CORREO }, config.jwtSecret, {
        expiresIn: "24h",
      });

      if (this.validCreditNumber) {
        await sheet.addRow({ CORREO, TIENDA: stores });

        res.status(201).json({
          message: "Cuenta de usuario nuevo agregada",
          token,
          userId: id,
          correo: CORREO,
        });
      }
    } catch (error) {
      console.log(error);
      return res.status(400).json(error);
    }
  }

  //UPDATE CREDITS
  async updateCreditAccount(req: Request, res: Response) {
    const { id } = req.params;
    const stores = JSON.parse(<string>req.headers["store"]);
    let validStore = [];
    try {
      const documento = new GoogleSpreadsheet(this.googleId);
      await documento.useServiceAccountAuth(credenciales);
      await documento.loadInfo();

      const sheet = documento.sheetsByIndex[0];

      const range = `A${id}:C${id}`;
      await sheet.loadCells(range);

      const a = sheet.getCellByA1(`A${id}`); // or A1 style notation
      const b = sheet.getCellByA1(`B${id}`);
      //console.log(sheet.cellStats);

      const { CORREO } = req.body;

      if (a.value === CORREO) {
        const serverStores = JSON.parse(b.value);

        serverStores.map((backstore: any) => {
          stores.map((frontstore: any) => {
            if (backstore.tienda === frontstore.tienda) {
              const montoFinal =
                Math.sign(frontstore.credito) === 1
                  ? backstore.credito +
                    Math.sign(frontstore.credito) * frontstore.credito
                  : backstore.credito -
                      Math.sign(frontstore.credito) * frontstore.credito <
                    0
                  ? this.runOutOfCredits(frontstore, backstore, res)
                  : backstore.credito -
                    Math.sign(frontstore.credito) * frontstore.credito;
              console.log(montoFinal);

              const { tienda } = backstore;

              validStore.push({ tienda, credito: montoFinal });
            }
          });
        });
      } else {
        this.valid = false;
        res.status(400).json({
          message:
            "El correo ingresado no esta registrado o es erroneo, porfavor intentelo nuevamente",
          error: "ERROR",
        });
        return;
      }

      if (validStore.length === 0) {
        this.valid = false;
        res.status(400).json({
          message: "La tienda no esta registrada porfavor, intento nuevamente",
          error: "ERROR",
        });
        return;
      }
      // passing values
      a.value = CORREO;
      b.value = JSON.stringify(validStore);

      if (this.valid) {
        await sheet.saveUpdatedCells();
      }

      res.status(201).json({
        message: `Cuenta de usuario ${id} actualizada`,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json(error);
    }
  }
  // READ CREDITS ACCOUNT
  async readCreditAccountService(req: Request, res: Response) {
    let Rows = [];

    try {
      const documento = new GoogleSpreadsheet(this.googleId);
      await documento.useServiceAccountAuth(credenciales);
      await documento.loadInfo();

      const sheet = documento.sheetsByIndex[0];
      const registro = await sheet.getRows();

      const response = JSON.stringify(registro, this.getCircularReplacer());

      let json = JSON.parse(response);

      json.map((res, i) => {
        const { _rawData, _sheet, _rowNumber } = res;
        //const [col1, col2, col3] = _sheet.headerValues;
        const [CORREO, TIENDA] = _rawData;
        const correo = req.body.CORREO;
        const store = JSON.parse(TIENDA);

        if (CORREO === correo) {
          Rows.push({ id: _rowNumber, CORREO, TIENDA: store });
        }
      });

      if (Rows.length === 0) {
        res.status(404).json({
          message: "Usuario no registrado",
          error: "ERROR",
        });
        return;
      }

      res.send(Rows);
    } catch (error) {
      console.log(error);
      res.status(400).json({
        message: "Algo ha ido mal, porfavor intentelo nuevamente",
        error: "ERROR",
      });
      return;
    }
  }
  //VALIDATE EMAIL ACCOUNT
  async validateEmailAccount(req: Request, res: Response) {
    let Rows: any[] = [];
    try {
      const documento = new GoogleSpreadsheet(this.googleId);
      await documento.useServiceAccountAuth(credenciales);
      await documento.loadInfo();

      const sheet = documento.sheetsByIndex[0];
      const registro = await sheet.getRows();

      const response = JSON.stringify(registro, this.getCircularReplacer());

      let json = JSON.parse(response);

      json.map((res, i) => {
        const { _rawData, _sheet, _rowNumber } = res;
        //const [col1, col2, col3] = _sheet.headerValues;
        const [CORREO] = _rawData;
        const correo = req.body.CORREO;

        if (CORREO === correo) {
          // token
          const token = jwt.sign(
            { userId: _rowNumber, CORREO },
            config.jwtSecret,
            {
              expiresIn: "24h",
            }
          );

          Rows.push({ id: _rowNumber, CORREO, token });
        }
      });
      if (Rows.length === 0) {
        res.status(400).json({
          message:
            "El correo no esta registrado, porfavor crear un nuevo Usuario",
          error: "ERROR",
        });
        return;
      }
      // return token and user id and email
      return Rows;
    } catch (error) {
      console.log(error);
      res.status(400).json({
        message: "Algo ha ido mal, porfavor intentelo nuevamente",
        error: "ERROR",
      });
      return;
    }
  }
  runOutOfCredits(frontstore: any, backstore: any, res: Response) {
    this.valid = false;
    res.status(400).json({
      message: `Error en el descuento de ${frontstore.credito} es superior al guardado en sistema ${backstore.credito}, porfavor intentalo nuevamente.`,
      error: "ERROR",
    });
    return;
  }

  getCircularReplacer() {
    const seen = new WeakSet();
    return (key: any, value: string) => {
      if (typeof value === "object" && value !== null) {
        if (seen.has(value)) {
          return;
        }
        seen.add(value);
      }
      return value;
    };
  }
}

export class userClass extends userService {
  constructor(googleId: string, req: Request, res: Response) {
    super(googleId, req, res);
  }
  //GELL ALL DATA FROM USERS
  all_users(req: Request, res: Response) {
    try {
      this.getAllUsers(req, res);
    } catch (error) {
      res.status(400).json({ message: "error: " + error });
    }
  }

  // SAVE NEW USER ACCOUNT
  new_user(req: Request, res: Response) {
    try {
      this.saveNewUser(req, res);
    } catch (error) {
      res.status(400).json({ message: "error: " + error });
    }
  }
  // UPDATE USER ACCOUNT
  update_credit(req: Request, res: Response) {
    try {
      this.updateCreditAccount(req, res);
    } catch (error) {
      res.status(400).json({ message: "error: " + error });
    }
  }
  // READ CREDITS
  read_credits(req: Request, res: Response) {
    try {
      this.readCreditAccountService(req, res);
    } catch (error) {
      res.status(400).json({ message: "error: " + error });
    }
  }

  async validate_Login_Email(req: Request, res: Response) {
    try {
      const response = await this.validateEmailAccount(req, res);
      return response;
    } catch (error) {
      res.status(400).json({ message: "error: " + error });
    }
  }
}
