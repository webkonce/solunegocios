/**
 * @author Francisco Roa <franroav@webkonce.cl>
 */
/***FICHERO DE RUTAS */
import { Router } from "express";
import auth from "./auth";
import user from "./user";
import credits from "./credit";
import tests from "./tests";

const routes = Router();

routes.use("/auth", auth);
routes.use("/users", user);
routes.use("/consultar", credits);
routes.use("/test", tests);

export default routes;
