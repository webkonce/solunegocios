/**
 * @author Francisco Roa <franroav@webkonce.cl>
 */
import { testController } from "../controller/TestController";
import { Router } from "express";
import { checkJwt } from "../middleware/jwt";

const router = Router();

// get one user
router.get("/test_one", testController.testgetAllUserAccounts);

export default router;
