/**
 * @author Francisco Roa <franroav@webkonce.cl>
 */
import { UserController } from "./../controller/UserController";
import { Router } from "express";
import { checkJwt } from "../middleware/jwt";
//import { checkRole } from "../middleware/role";

const router = Router();

//Get all users account
router.get("/", [checkJwt], UserController.getAll);

// create a new User account
router.post("/", [checkJwt], UserController.saveNewUserAccount);

// Edit user account (agregar, descontar)
router.put("/:id", [checkJwt], UserController.editUserAccount);

export default router;
