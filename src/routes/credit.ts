/**
 * @author Francisco Roa <franroav@webkonce.cl>
 */
import { UserController } from "../controller/UserController";
import { Router } from "express";
import { checkJwt } from "../middleware/jwt";

const router = Router();

// get one user
router.get("/creditos", UserController.readUserCredits);

export default router;
