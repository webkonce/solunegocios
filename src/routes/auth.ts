/**
 * @author Francisco Roa <franroav@webkonce.cl>
 */
import { Router } from "express";
import AuthController from "../controller/AuthController";
import { checkJwt } from "../middleware/jwt";
const router = Router();

// login
router.post("/login", AuthController.login);

export default router;
