/**
 * @author Francisco Roa <franroav@webkonce.cl>
 */
import { Request, Response } from "express";

export class UserClass {
  constructor(
    private googleId: string,
    public req: Request,
    public res: Response
  ) {}
}

export interface User {
  id: number;
  CORREO: string;
  TIENDA: Array<{ tienda: number; credito: number }>;
}

/*
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  Unique,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";

import { MinLength, IsNotEmpty, IsEmail } from "class-validator";

import * as bcrypt from "bcryptjs";

@Entity()
@Unique(["username"])
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @MinLength(6)
  @IsEmail()
  @IsNotEmpty()
  username: string;

  @Column()
  @MinLength(6)
  @IsNotEmpty()
  password: string;

  @Column()
  @IsNotEmpty()
  role: string;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updateAt: Date;

  hashPassword(): void {
    const salt = bcrypt.genSaltSync(10);

    this.password = bcrypt.hashSync(this.password, salt);
  }

  checkPassword(password: string): boolean {
    return bcrypt.compareSync(password, this.password);
  }
}
*/
