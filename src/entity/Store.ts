export interface Store {
  CORREO: string;
  MONTO: string;
  TIENDA: string;
}

interface EnumCreditService {
  [TIENDA: string]: { tienda: number; credito: number };
}
