/**
 * @author Francisco Roa <franroav@webkonce.cl>
 */
import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";
import config from "../config/config";

export const checkJwt = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const token = <string>req.headers["auth"];
  let jwtPayload;

  try {
    jwtPayload = <any>jwt.verify(token, config.jwtSecret);
    res.locals.jwtPayload = jwtPayload;
  } catch (error) {
    return res.status(401).json({ message: "Usuario No Autorizado" });
  }

  // console.log("req", jwtPayload);
  const { id, CORREO } = jwtPayload;
  // token
  const newtoken = jwt.sign({ userId: id, CORREO }, config.jwtSecret, {
    expiresIn: "24h",
  });

  res.setHeader("token", newtoken);

  next();
};
