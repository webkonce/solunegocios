# solunegocios Api

Desafio solunegocios.

## replace credentials

from the root directory go to ./src/config/json/credenciales.json and replace the file credenciales.json with the one in the link below.

## Clone Project

1.- Open your terminal and got the folder wehre you wat to download the project. then execute the following command `git clone https://webkonce-admin@bitbucket.org/webkonce/solunegocios.git`, this will download the project into your local machine

## Install dependencies

1.- In your terminal, in the root directory of the project that you recently clone, where is you package.json. Run the following command `npm i` to install the dependencies.

## Start Server.

1.- After install all your dependencies, you are ready to start the server. Run the following command `npm run dev`. This script you can find it on the package.json file in the script property.
